#include<stdio.h>

int fact(int n)
{
   if(n==1)
       return 1;
   else
       return(n*fact(n-1));
}

int main()
{
    int x, y, n, i;
    scanf("%d", &x);

    for(i=1; i<=x; ++i){
        scanf("%d", &n);
        if(n==0)
            printf("Case %d: 1\n", i);
        else
            printf("Case %d: %d\n", i, fact(n));
    }
    return 0;
}


